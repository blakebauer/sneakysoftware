﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using Elimination.Controllers.Api;
using Elimination.Models;
using Elimination.Repositories;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Test
{
    [Binding]
    public sealed class APIGameControllerSteps
    {
        private Mock<IRepoManager> _repoManager;
        private Mock<IPlayerRepository> _playerMock;

        private List<Game> _games;
        private readonly GamesController _apiGamesController;

        private readonly GeneralResponse _noGamesFoundResponse;
        private readonly GeneralResponse _localResponseObj;
        private GeneralResponse _apiResponse;

        private Player _player;
        private Player _playerInGame;
        private Game _game;

        private readonly Player _nullPlayer = null;
        private readonly Game _nullGame = null;
        private readonly List<Game> _nullGameList = null;

        //variables
        private int gameId;
        private int playerId;
        private string ids;
        private readonly string[] _idArray = new[] {"1", "20", "40"};

        public APIGameControllerSteps()
        {
            _games = new List<Game>();
            _player = new Player()
            {
                PlayerID = 200,
                FirstName = "Alex",
                LastName = "Molodyh",
                Email = "almania@gmail.com",
                UserName = "almania",
                AuthUserID = "sldihaowesihjoanmsogfia",
                DOB = DateTime.Now.Date,
                Profile = "Hello there"
            };

            _playerInGame = new Player()
            {
                PlayerID = 400,
                FirstName = "Alex4",
                LastName = "Molodyh4",
                Email = "almania4@gmail.com",
                UserName = "almania4",
                AuthUserID = "sldihaowesihjoanmsogfia4",
                DOB = DateTime.Now.Date,
                Profile = "Hello there4"
            };

            _game = new Game()
            {
                GameID = 2000,
                Name = "My Game",
                AccesLevel = true,
                Active = false,
                Location = "Salem",
                Players = new List<Player>()
            };

            _noGamesFoundResponse = new GeneralResponse()
            {
                Name = "API GetGames Call",
                Error = $"There are no games in the database."
            };

            _localResponseObj = new GeneralResponse();

            _playerMock = new Mock<IPlayerRepository>();
            _repoManager = new Mock<IRepoManager>();

            _playerMock.Setup(pm => pm.GetPlayers()).Returns(GetPlayers);
            _playerMock.Setup(pm => pm.GetPlayerByID(200)).Returns(_player);
            _playerMock.Setup(pm => pm.GetPlayerByID(400)).Returns(_playerInGame);
            _playerMock.Setup(pm => pm.GetPlayerByIdAsync(2)).Returns(Task.FromResult(_nullPlayer));

            _repoManager.Setup(rm => rm.Games.GetGames()).Returns(_games);
            _repoManager.Setup(rm => rm.Games.GetGameByIdAsync(20)).Returns(Task.FromResult(_nullGame));
            _repoManager.Setup(rm => rm.Games.GetGameListByIdsAsync(_idArray)).Returns(Task.FromResult(_nullGameList));

            //JoinGame stuff
            _repoManager.Setup(rm => rm.Games.GetGameByID(2000)).Returns(_game);
            _repoManager.Setup(rm => rm.Games.GetGameByID(400)).Returns(_nullGame);
            _repoManager.Setup(rm => rm.Games.GetGameByID(300)).Returns(_nullGame);
            _repoManager.Setup(rm => rm.Games.GetGameByID(200)).Returns(_nullGame);
            _repoManager.Setup(rm => rm.Games.GetGameByID(1)).Returns(_nullGame);

            _repoManager.Setup(rm => rm.Players.GetPlayerByID(200)).Returns(_player);
            _repoManager.Setup(rm => rm.Players.GetPlayerByID(400)).Returns(_playerInGame);


            _repoManager.Setup(rm => rm.Players).Returns(_playerMock.Object);

            //Create API GamesController
            _apiGamesController = new GamesController(_repoManager.Object);
        }

        #region Tests with empty games

        #region Test GetGames with no games in the database

        [Given(@"There are no games in the database")]
        public void GivenThereAreNoGamesInTheDatabase()
        {
            _games?.Clear();
        }

        [When(@"I call GetGames")]
        public void CallGetGames()
        {
            var temp = _apiGamesController.GetGames() as JsonResult<GeneralResponse>;
            _apiResponse = temp?.Content;
        }

        [Then(@"the result should be an error that says there are no games in the database")]
        public void ThenTheResultShouldBeAnErrorThatSaysThereAreNoGamesInTheDatabase()
        {
            Assert.That(_apiResponse.Error.Equals(_noGamesFoundResponse.Error));
            Assert.That(_apiResponse.Name.Equals(_noGamesFoundResponse.Name));
        }


        #endregion

        #region Test GetGamesLite with no games in the database
        
        [Given(@"There are no games in the database for GetGamesLite")]
        public void GivenThereAreNoGamesInTheDatabaseForGetGamesLite()
        {
            _games?.Clear();
        }

        [When(@"I call GetGamesLite")]
        public void WhenICallGetGamesLite()
        {
            var temp = _apiGamesController.GetGamesLite() as JsonResult<GeneralResponse>;
            _apiResponse = temp.Content;
        }

        [Then(@"the result from GetGamesLite should be an error that says there are no games in the database")]
        public void ThenTheResultFromGetGamesLiteShouldBeAnErrorThatSaysThereAreNoGamesInTheDatabase()
        {
            _noGamesFoundResponse.Name = "API GetGamesLite Call";
            
            Assert.That(_apiResponse.Error.Equals(_noGamesFoundResponse.Error));
            Assert.That(_apiResponse.Name.Equals(_noGamesFoundResponse.Name));
        }

        #endregion

        #endregion

        [Given(@"I set gameID in GetGameLite to (.*)")]
        public void GivenISetGameIDInGetGameLiteTo(int p0)
        {
            gameId = p0;
        }

        [Given(@"I set host PlayerID in GetHostGames to (.*)")]
        public void GivenISetHostPlayerIDInGetHostGamesTo(int p0)
        {
            playerId = p0;
        }

        [Given(@"I set host PlayerID GetHostGamesTwo to (.*)")]
        public void GivenISetHostPlayerIDGetHostGamesTwoTo(int p0)
        {
            playerId = p0;
        }

        [Given(@"I set gameId in GetAllPlayers to (.*)")]
        public void GivenISetGameIdInGetAllPlayersTo(int p0)
        {
            gameId = p0;
        }

        [Given(@"I set gameId in JoinGameWithBadGameId to (.*)")]
        public void GivenISetGameIdInJoinGameWithBadGameIdTo(int p0)
        {
            gameId = p0;
        }

        [Given(@"I set playerId in JoinGameWithBadGameId to (.*)")]
        public void GivenISetPlayerIdInJoinGameWithBadGameIdTo(int p0)
        {
            playerId = p0;
        }

        [Given(@"I set gameId in JoinGameWithBadPlayerId to (.*)")]
        public void GivenISetGameIdInJoinGameWithBadPlayerIdTo(int p0)
        {
            gameId = p0;
        }

        [Given(@"I set playerId in JoinGameWithBadPlayerId to (.*)")]
        public void GivenISetPlayerIdInJoinGameWithBadPlayerIdTo(int p0)
        {
            playerId = p0;
        }

        [Given(@"I set gameId in JoinGameWithBadPlayerIdAndGameId to (.*)")]
        public void GivenISetGameIdInJoinGameWithBadPlayerIdAndGameIdTo(int p0)
        {
            gameId = p0;
        }

        [Given(@"I set playerId in JoinGameWithBadPlayerIdAndGameId to (.*)")]
        public void GivenISetPlayerIdInJoinGameWithBadPlayerIdAndGameIdTo(int p0)
        {
            playerId = p0;
        }

        [Given(@"I set gameId in JoinGameWithExistingPlayerInGame to (.*)")]
        public void GivenISetGameIdInJoinGameWithExistingPlayerInGameTo(int p0)
        {
            gameId = p0;
        }

        [Given(@"I set gameId in JoinGameAsHost to (.*)")]
        public void GivenISetGameIdInJoinGameAsHostTo(int p0)
        {
            gameId = p0;
        }

        [Given(@"I set playerId in JoinGameAsHost to (.*)")]
        public void GivenISetPlayerIdInJoinGameAsHostTo(int p0)
        {
            playerId = p0;
        }

        [Given(@"I set gameID to (.*)")]
        public void GivenISetGameIDTo(int p0)
        {
            gameId = p0;
        }

        [Given(@"I set string ids to ""(.*)""")]
        public void GivenISetStringIdsTo(string p0)
        {
            ids = p0;
        }

        [Given(@"player is already in game")]
        public void GivenPlayerIsAlreadyInGame()
        {
            _game.Players.Add(_playerInGame);
        }

        [Given(@"player is the host of the game")]
        public void GivenPlayerIsTheHostOfTheGame()
        {
            _game.Host = _player.PlayerID;
        }

        [Given(@"I set playerId in JoinGameWithExistingPlayerInGame to (.*)")]
        public void GivenISetPlayerIdInJoinGameWithExistingPlayerInGameTo(int p0)
        {
            playerId = p0;
        }

        [When(@"I call GetGame with gameID")]
        public void WhenICallGetGameWithGameID()
        {
            var temp = _apiGamesController.GetGame(gameId);
            _apiResponse = (temp.Result as JsonResult<GeneralResponse>)?.Content;
        }

        [When(@"I call GetGameLite with gameID")]
        public void WhenICallGetGameLiteWithGameID()
        {
            var temp = _apiGamesController.GetGameLite(gameId);
            _apiResponse = (temp.Result as JsonResult<GeneralResponse>)?.Content;
        }

        [When(@"I call GetHostGames with PlayerID")]
        public void WhenICallGetHostGamesWithPlayerID()
        {
            var temp = _apiGamesController.GetHostGames(playerId) as JsonResult<GeneralResponse>;
            _apiResponse = temp.Content;
        }

        [When(@"I call GetGameList with ids")]
        public void WhenICallGetGameListWithIds()
        {
            var temp = _apiGamesController.GetGameList(ids);
            _apiResponse = (temp.Result as JsonResult<GeneralResponse>)?.Content;
        }

        [When(@"I call GetAllPlayers with id")]
        public void WhenICallGetAllPlayersWithId()
        {
            var temp = _apiGamesController.GetAllPlayers(gameId);
            _apiResponse = (temp.Result as JsonResult<GeneralResponse>)?.Content;
        }

        [When(@"I call JoinGame with gameId and playerId")]
        public void WhenICallJoinGameWithGameIdAndPlayerId()
        {
            var temp = _apiGamesController.JoinGame(gameId, playerId);
            _apiResponse = (temp.Result as JsonResult<GeneralResponse>)?.Content;
        }

        [When(@"I call JoinGame with gameId and playerId as the host")]
        public void WhenICallJoinGameWithGameIdAndPlayerIdAsTheHost()
        {
            var temp = _apiGamesController.JoinGame(gameId, playerId);
            _apiResponse = (temp.Result as JsonResult<GeneralResponse>)?.Content;
        }

        [When(@"I call JoinGame with gameId and playerId and player is already in game")]
        public void WhenICallJoinGameWithGameIdAndPlayerIdAndPlayerIsAlreadyInGame()
        {
            var temp = _apiGamesController.JoinGame(gameId, playerId);
            _apiResponse = (temp.Result as JsonResult<GeneralResponse>)?.Content;
        }

        [Then(@"the error from GetGameWithGameId should say GameID (.*) does not exist in the database")]
        public void ThenTheErrorFromGetGameWithGameIdShouldSayGameIDDoesNotExistInTheDatabase(int p0)
        {
            _localResponseObj.Name = "API GetGame Call";
            _localResponseObj.Error = $"GameID {p0} does not exist in the database.";

            Assert.That(_apiResponse.Name.Equals(_localResponseObj.Name));
            Assert.That(_apiResponse.Error.Equals(_localResponseObj.Error));
        }

        [Then(@"the error from GetGameLiteWithGameId should say GameID (.*) does not exist in the database")]
        public void ThenTheErrorFromGetGameLiteWithGameIdShouldSayGameIDDoesNotExistInTheDatabase(int p0)
        {
            _localResponseObj.Name = "API GetGameLite Call";
            _localResponseObj.Error = $"GameID {p0} does not exist in the database.";

            Assert.That(_apiResponse.Name.Equals(_localResponseObj.Name));
            Assert.That(_apiResponse.Error.Equals(_localResponseObj.Error));
        }

        [Then(@"the error from GetHostGamesByPlayerID should say There is no host with id of (.*)")]
        public void ThenTheErrorFromGetHostGamesByPlayerIDShouldSayThereIsNoHostWithIdOf(int p0)
        {
            _localResponseObj.Name = "API GetHostGames Call";
            _localResponseObj.Error = $"There is no host with id of {p0}.";

            Assert.That(_apiResponse.Name.Equals(_localResponseObj.Name));
            Assert.That(_apiResponse.Error.Equals(_localResponseObj.Error));
        }

        [Then(@"the error from GetHostGamesByPlayerIDTwo should say There are no games for the host with id of (.*)")]
        public void ThenTheErrorFromGetHostGamesByPlayerIDTwoShouldSayThereAreNoGamesForTheHostWithIdOf(int p0)
        {
            _localResponseObj.Name = "API GetHostGames Call";
            _localResponseObj.Error = $"There are no games for the host with id of {p0}.";

            Assert.That(_apiResponse.Name.Equals(_localResponseObj.Name));
            Assert.That(_apiResponse.Error.Equals(_localResponseObj.Error));
        }

        [Then(@"The error from GetGameList should say There are no games for these id's: ""(.*)""")]
        public void ThenTheErrorFromGetGameListShouldSayThereAreNoGamesForTheseIdS(string p0)
        {
            _localResponseObj.Name = "API GetGameList Call";
            _localResponseObj.Error = $"There are no games for these id's: {p0}.";

            Assert.That(_apiResponse.Name.Equals(_localResponseObj.Name));
            Assert.That(_apiResponse.Error.Equals(_localResponseObj.Error));
        }

        [Then(@"the error from GetAllPlayersForGame should be There is no game with the GameID of (.*)")]
        public void ThenTheErrorFromGetAllPlayersForGameShouldBeThereIsNoGameWithTheGameIDOf(int p0)
        {
            _localResponseObj.Name = "API GetAllPlayers Call";
            _localResponseObj.Error = $"There is no game with the GameID of {p0}.";

            Assert.That(_apiResponse.Name.Equals(_localResponseObj.Name));
            Assert.That(_apiResponse.Error.Equals(_localResponseObj.Error));
        }

        [Then(@"the error from JoinGameWithBadGameId should be There is no game with the GameID of: (.*)\.")]
        public void ThenTheErrorFromJoinGameWithBadGameIdShouldBeThereIsNoGameWithTheGameIDOf_(int p0)
        {
            _localResponseObj.Name = "API JoinGame Call";
            _localResponseObj.Error = $"There is no game with the GameID of: {p0}.";

            Assert.That(_apiResponse.Name.Equals(_localResponseObj.Name));
            Assert.That(_apiResponse.Error.Equals(_localResponseObj.Error));
        }

        [Then(@"the error from JoinGameWithBadPlayerId should be There is no player with PlayerID of: (.*)\.")]
        public void ThenTheErrorFromJoinGameWithBadPlayerIdShouldBeThereIsNoPlayerWithPlayerIDOf_(int p0)
        {
            _localResponseObj.Name = "API JoinGame Call";
            _localResponseObj.Error = $"There is no player with PlayerID of: {p0}.";

            Assert.That(_apiResponse.Name.Equals(_localResponseObj.Name));
            Assert.That(_apiResponse.Error.Equals(_localResponseObj.Error));
        }

        [Then(@"the error from JoinGameWithBadPlayerIdAndGameId should be Name ""(.*)"" and Error ""(.*)"";")]
        public void ThenTheErrorFromJoinGameWithBadPlayerIdAndGameIdShouldBeNameAndError(string p0, string p1)
        {
            Assert.That(_apiResponse.Name.Equals(p0));
            Assert.That(_apiResponse.Error.Equals(p1));
        }

        [Then(@"the error from JoinGameWithExistingPlayerInGame should be Name ""(.*)"" and Error ""(.*)""\.")]
        public void ThenTheErrorFromJoinGameWithExistingPlayerInGameShouldBeNameAndError_(string p0, string p1)
        {
            Assert.That(_apiResponse.Name.Equals(p0));
            Assert.That(_apiResponse.Error.Equals(p1));
        }

        [Then(@"the error from JoinGameAsHost should be Name ""(.*)"" and Error ""(.*)""")]
        public void ThenTheErrorFromJoinGameAsHostShouldBeNameAndError(string p0, string p1)
        {
            Assert.That(_apiResponse.Name.Equals(p0));
            Assert.That(_apiResponse.Error.Equals(p1));
        }

        private List<Player> GetPlayers()
        {
            return new List<Player>()
            {
                new Player()
                {
                    PlayerID = 1,
                    FirstName = "me",
                    LastName = "you",
                    Profile = "Stuff",
                    DOB = DateTime.Now.Date,
                    UserName = "meyou"
                },
                new Player()
                {
                    PlayerID = 2,
                    FirstName = "me2",
                    LastName = "you2",
                    Profile = "Stuff2",
                    DOB = DateTime.Now.Date,
                    UserName = "meyou2"
                }
            };
        }
    }
}
