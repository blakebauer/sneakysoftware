﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace Test.App_Start
{
    [Binding]
    public sealed class ID286_ErrorPage_Steps
    {
        private RestClient client = new RestClient();
        private string res;

        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef

        [Given("I'm at the hompage navigating to non existing page")]
        public void StartAtHome()
        {
            client = new RestClient("http://localhost:1842/alkdsjflkdjfk");
        }


        [When("I hit enter")]
        public void WhenIPressEnter()
        {
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request).Content;
            res = response.ToString();
        }

        [Then("I should be returned my custom error")]
        public void ThenTheResultShouldBe()
        {
            var ErrorMessage = res;
            bool check = ErrorMessage.Contains("Oops..");
            Assert.That(check.Equals(true));
        }

        [Given("I'm at the hompage navigating to an existing page")]
        public void TryToGoToExistingPage()
        {
            client = new RestClient("http://localhost:1842/Home/About");
        }

        [When("I press enter")]
        public void HitEnterForExistingPage()
        {
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request).Content;
            res = response.ToString();
        }

        [Then("I should not get my error page")]
        public void ShouldNotSeeErrorPage()
        {
            var ErrorMessage = res;
            bool check = ErrorMessage.Contains("Oops..");
            Assert.That(check.Equals(false));
        }


        [Given(@"I try visiting the ""(.*)"" page without logging in")]
        public void GotToGamesWithoutLoggingIn(string p0)
        {
            client = new RestClient("http://localhost:1842/Games");

        }
        [When("I ty to view the page")]
        public void EnterWithoutLogin()
        {
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request).Content;
            res = response.ToString();
        }
        [Then ("I should be taken to my error page")]
        public void ShouldSeePleasLoginPage()
        {
            var ErrorMessage = res;
            bool check = ErrorMessage.Contains("Please log in or register");
            Assert.That(check.Equals(true));
        }


        [Given(@"I try visiting the ""(.*)"" for a game that is not mine")]
        public void GameThatIsNotMine(string p0)
        {
            client = new RestClient("https://elimination.azurewebsites.net/Games/HostDetails/2");
        }
        [When ("I ty to view the HostDetails page")]
        public void ViewHostDetails()
        {
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request).Content;
            res = response.ToString();
        }
        [Then ("I should go to my error page")]
        public void HostDetailsError()
        {
            var ErrorMessage = res;
            bool check = ErrorMessage.Contains("Sorry not your game");
            Assert.That(check.Equals(true));

        }

    }
}
