﻿using Elimination.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    [TestFixture]
    class GameTargetTests
    {
        Game game;
        Player[] players;

        [SetUp]
        public void SetUpPlayer()
        {
            game = new Game() {
                GameID = 10,
                Name = "Game1"
            };
            game.GameID = 10;

            players = new Player[] {
                new Player(){PlayerID = 1},
                new Player(){PlayerID = 2},
                new Player(){PlayerID = 3},
                new Player(){PlayerID = 4},
                new Player(){PlayerID = 5}
            };
        }

        [Test]
        public void Test_TwoPlayerGameGetsTargetsToEachOther()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.StartGame();

            Assert.That(game.Targets.Any(t => t.Player == players[0].PlayerID));
            Assert.That(game.Targets.Any(t => t.TargetID == players[0].PlayerID));
            Assert.That(game.Targets.Any(t => t.Player == players[1].PlayerID));
            Assert.That(game.Targets.Any(t => t.TargetID == players[1].PlayerID));
            Assert.That(game.Targets.FirstOrDefault(t => t.Player == players[0].PlayerID).TargetID == players[1].PlayerID);
            Assert.That(game.Targets.FirstOrDefault(t => t.Player == players[1].PlayerID).TargetID == players[0].PlayerID);
        }

        [Test]
        public void Test_EliminatingAPlayerReturnsProperObject()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.StartGame();
            Elimination.Models.Elimination e = game.EliminatePlayer(players[1].PlayerID);

            Assert.That(e.Eliminator, Is.EqualTo(players[0].PlayerID));
            Assert.That(e.Eliminated, Is.EqualTo(players[1].PlayerID));
            Assert.That(e.GameID, Is.EqualTo(game.GameID));
        }

        [Test]
        public void Test_GameBadgesCheckWinConditionReturnsCorrectly()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            game.EliminatePlayer(players[1].PlayerID);

            Assert.That(GameBadges.CheckWinCondition(players[0], game), Is.False);
            Assert.That(GameBadges.CheckWinCondition(players[1], game), Is.False);
            Assert.That(GameBadges.CheckWinCondition(players[2], game), Is.False);

            game.EliminatePlayer(players[0].PlayerID);

            Assert.That(GameBadges.CheckWinCondition(players[0], game), Is.False);
            Assert.That(GameBadges.CheckWinCondition(players[1], game), Is.False);
            Assert.That(GameBadges.CheckWinCondition(players[2], game), Is.True);
        }

        [Test]
        public void Test_GameHasWinnerAndWinnerIsCorrect()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            game.EliminatePlayer(players[1].PlayerID);

            Assert.That(game.Winner == null, $"Game winner should be null but was {game.Winner}");

            game.EliminatePlayer(players[0].PlayerID);

            Assert.That(game.Winner == players[2].PlayerID, $"Game winner should be {players[2].PlayerID} but was {game.Winner}");
        }

        [Test]
        public void Test_GameRemovePlayerInWinConditionSetsCorrectWinner()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            game.EliminatePlayer(players[1].PlayerID);

            Assert.That(game.Winner == null, $"Game winner should be null but was {game.Winner}");

            game.RemovePlayer(players[0].PlayerID);

            Assert.That(game.Winner == players[2].PlayerID, $"Game winner should be {players[2].PlayerID} but was {game.Winner}");
        }

        [Test]
        public void Test_BadgeID24_WinAndKillOnePlayer_IfPlayerWinsGameWithMoreThenOneKillTheyAreNotAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            Target t = game.Targets.FirstOrDefault(td => td.Player == players[0].PlayerID);
            Target t2 = game.Targets.FirstOrDefault(td => td.Player == t.TargetID);
            game.EliminatePlayer(players[0].PlayerID);
            game.EliminatePlayer(t.TargetID);

            // Assert setup is correct
            Assert.That(game.Eliminations.Where(e => e.Eliminator == t2.TargetID).Count() == 2, "Setup is not correct.");

            // Assert Badge is not awarded
            Assert.That(players[t2.TargetID - 1].PlayerBadges.Any(b => b.BadgeID == 24) == false, "Badge was awarded when it should not be.");
        }

        [Test]
        public void Test_BadgeID24_WinAndKillOnePlayer_IfPlayerWinsGameWithOneKillTheyAreAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            Target t = game.Targets.FirstOrDefault(td => td.Player == players[0].PlayerID);
            Target t2 = game.Targets.FirstOrDefault(td => td.Player == t.TargetID);
            game.EliminatePlayer(players[0].PlayerID);
            game.EliminatePlayer(t2.TargetID);

            // Assert setup is correct
            Assert.That(game.Eliminations.Where(e => e.Eliminator == t2.Player).Count() == 1, "Setup is not correct.");

            // Assert Badge is awarded
            Assert.That(players[t2.Player - 1].PlayerBadges.Any(b => b.BadgeID == 24), Is.True);
        }

        [Test]
        public void Test_BadgeID24_WinAndKillOnePlayer_IfPlayerKillsOnePlayerAndDiesTheyAreNotAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            Target t = game.Targets.FirstOrDefault(td => td.TargetID == players[0].PlayerID);
            game.EliminatePlayer(players[0].PlayerID);
            game.EliminatePlayer(t.Player);

            Assert.That(players[t.TargetID - 1].PlayerBadges.Any(b => b.BadgeID == 24), Is.False);
        }

        [Test]
        public void Test_BadgeID25_WinAndKillNoPlayers_IfPlayerWinsGameWithNoKillsTheyAreAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            Target t = game.Targets.FirstOrDefault(td => td.Player == players[0].PlayerID);
            Target t2 = game.Targets.FirstOrDefault(td => td.Player == t.TargetID);
            game.EliminatePlayer(players[0].PlayerID);
            game.RemovePlayer(t2.TargetID);

            // Assert setup is correct
            Assert.That(game.Eliminations.Where(e => e.Eliminator == t2.Player).Count() == 0, "Setup is not correct.");

            // Assert Badge is awarded
            Assert.That(players[t2.Player - 1].PlayerBadges.Any(b => b.BadgeID == 25), Is.True);
        }

        [Test]
        public void Test_BadgeID25_WinAndKillNoPlayers_IfPlayerWinsGameWithMoreThenZeroKillsTheyAreNotAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            Target t = game.Targets.FirstOrDefault(td => td.Player == players[0].PlayerID);
            Target t2 = game.Targets.FirstOrDefault(td => td.Player == t.TargetID);
            game.EliminatePlayer(players[0].PlayerID);
            game.EliminatePlayer(t2.TargetID);

            // Assert setup is correct
            Assert.That(game.Eliminations.Where(e => e.Eliminator == t2.Player).Count() != 0, "Setup is not correct.");

            // Assert Badge is not awarded
            Assert.That(players[t2.Player - 1].PlayerBadges.Any(b => b.BadgeID == 25), Is.False);
        }

        [Test]
        public void Test_BadgeID26_WinAndKillAllPlayers_IfPlayerWinsGameWithAllKillsTheyAreAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            Target t = game.Targets.FirstOrDefault(td => td.Player == players[0].PlayerID);
            Target t2 = game.Targets.FirstOrDefault(td => td.Player == t.TargetID);
            game.EliminatePlayer(players[0].PlayerID);
            game.EliminatePlayer(t2.Player);

            // Assert setup is correct
            Assert.That(game.Eliminations.Where(e => e.Eliminator == t2.TargetID).Count() == 2, "Setup is not correct.");

            // Assert Badge is awarded
            Assert.That(players[t2.TargetID - 1].PlayerBadges.Any(b => b.BadgeID == 26), Is.True);
        }

        [Test]
        public void Test_BadgeID26_WinAndKillAllPlayers_IfPlayerWinsGameWithLessThenAllKillsTheyAreNotAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            Target t = game.Targets.FirstOrDefault(td => td.Player == players[0].PlayerID);
            Target t2 = game.Targets.FirstOrDefault(td => td.Player == t.TargetID);
            game.EliminatePlayer(players[0].PlayerID);
            game.EliminatePlayer(t2.TargetID);

            // Assert setup is correct
            Assert.That(game.Eliminations.Where(e => e.Eliminator == t2.Player).Count() == 1, "Setup is not correct.");

            // Assert Badge is not awarded
            Assert.That(players[t2.Player - 1].PlayerBadges.Any(b => b.BadgeID == 26), Is.False);
        }

        [Test]
        public void Test_BadgeID27_WinAGame_AfterPlayerWinsAGameTheyAreAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.StartGame();
            game.EliminatePlayer(players[1].PlayerID);

            Assert.That(players[0].PlayerBadges.Any(b => b.BadgeID == 27));
        }

        [Test]
        public void Test_BadgeID27_WinAGame_AfterPlayerLosesAGameTheyAreNotAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.StartGame();
            game.EliminatePlayer(players[1].PlayerID);

            Assert.That(players[1].PlayerBadges.Any(b => b.BadgeID == 27) == false);
        }

        [Test]
        public void Test_BadgeID28_FirstBlood_IfPlayerKillsTheFirstPersonThenBadgeIsAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.StartGame();
            game.EliminatePlayer(players[0].PlayerID);

            // Assert Badge is not awarded
            Assert.That(players[1].PlayerBadges.Any(b => b.BadgeID == 28), Is.True);
        }

        [Test]
        public void Test_BadgeID28_FirstBlood_IfPlayerDoesntKillTheFirstPersonThenBadgeIsNotAwarded()
        {
            game.Players.Add(players[0]);
            game.Players.Add(players[1]);
            game.Players.Add(players[2]);
            game.StartGame();
            Target t = game.Targets.FirstOrDefault(td => td.TargetID == players[0].PlayerID);
            Target t2 = game.Targets.FirstOrDefault(td => td.TargetID == t.Player);
            game.EliminatePlayer(players[0].PlayerID);
            game.EliminatePlayer(t.Player);

            // Assert Badge is not awarded
            Assert.That(players[t2.Player - 1].PlayerBadges.Any(b => b.BadgeID == 28), Is.False);
        }

        
        [TestCase(5, 29)]
        [TestCase(10, 30)]
        [TestCase(15, 31)]
        public void Test_BadgeID29to31_IncrementalEliminations_IfPlayerKillsnPeopleTheyGetBadgeb(int n, int b)
        {
            for(int i = 0; i < n+2; i++)
            {
                game.Players.Add(new Player() { PlayerID = i });
            }
            game.StartGame();
            
            for (int i = 0; i < n - 1; i++)
            {
                Target t = game.Targets.FirstOrDefault(td => td.Player == 0);
                game.EliminatePlayer(t.TargetID);
            }
            // Assert Badge is not awarded
            Assert.That(game.Players.FirstOrDefault(p => p.PlayerID == 0).PlayerBadges.Any(ba => ba.BadgeID == b), Is.False);

            Target ts = game.Targets.FirstOrDefault(td => td.Player == 0);
            game.EliminatePlayer(ts.TargetID);

            // Assert Badge is not awarded
            Assert.That(game.Players.FirstOrDefault(p => p.PlayerID == 0).PlayerBadges.Any(ba => ba.BadgeID == b), Is.True);
        }
    }
}
