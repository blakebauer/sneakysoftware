﻿using System;
using NUnit.Framework;
using Elimination.Controllers;
using System.Web.Mvc;
using Elimination.Models;
using Elimination.Repositories;
using Moq;

//PBI 197, Ratio helper
namespace Test
{
    [TestFixture]
    public class HM_UnitTests
    {
        private Mock<IRepoManager> repoManager = new Mock<IRepoManager>();
        
        [Test]
        public void Test_Ratio_if_both_one()
        {
        
           DashboardController c = new DashboardController(repoManager.Object);
            int targetsElim = 1;
            int timesElim = 1;
            int expectedResult = 1;

            int actualResult = c.Ratio(targetsElim, timesElim);
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Test_Ratio_if_timesEliminated_zero()
        {
            DashboardController c = new DashboardController(repoManager.Object);
            int targetsElim = 1;
            int timesElim = 0;
            int expectedResult = 1;

            int actualResult = c.Ratio(targetsElim, timesElim);
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Test_Ratio_if_targetsEliminated_zero()
        {
            DashboardController c = new DashboardController(repoManager.Object);
            int targetsElim = 0;
            int timesElim = 1;
            int expectedResult = 0;

            int actualResult = c.Ratio(targetsElim, timesElim);
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Test_Ratio_if_targetsEliminated_zero_and_timesEliminated_zero()
        {
            DashboardController c = new DashboardController(repoManager.Object);
            int targetsElim = 0;
            int timesElim = 0;
            int expectedResult = 0;

            int actualResult = c.Ratio(targetsElim, timesElim);
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }


    }
}
