﻿using Elimination.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    [TestFixture]
    class BB_UnitTests
    {
        Player p;

        [SetUp]
        public void SetUpPlayer()
        {
            p = new Player();
            p.UserName = "Bob314";
        }

        [Test]
        public void Test_BlobContainerNameIsGeneratedFromPlayerObject()
        {
            Assert.That(BasicStorage.GetContainerName(player: p), Is.EqualTo("bob314"));
        }

        [Test]
        public void Test_BlobContainerNameIsLowerCase()
        {
            Assert.That(BasicStorage.GetContainerName(player: p), Is.EqualTo("Bob314".ToLower()));
        }

        [Test]
        public void Test_BlobNameIsGeneratedFromPlayerObject()
        {
            Assert.That(BasicStorage.GetBlobName(".png", player: p), Is.EqualTo("bob314.png"));
        }

        [Test]
        public void Test_BlobNameExtraTextIsAppended()
        {
            Assert.That(BasicStorage.GetBlobName(".png", player: p, extraText: "_EliminPic1"), Is.EqualTo("bob314_EliminPic1.png"));
        }
    }
}
