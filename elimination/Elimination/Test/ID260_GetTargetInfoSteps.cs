﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace Test
{
    [Binding]
    public sealed class ID260_GetTargetInfoSteps
    {
        dynamic data;
        string email;
        private RestClient client = new RestClient();
        private RestClient knownEmail = new RestClient();


        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef

        /////////////////////////TEST-ONE//////////////////////////////////////

        [Given (@"I use API method GetTargetInfo without PlayerID")]
        public void UseGetTargetInfoNoID()
        {
            client = new RestClient("http://localhost:1842/api/Targets/GetYourTarget?id=");
        }
        [Then (@"error message should be recived")]
        public void GetErroNoIDr()
        {
            string ErrorMessage = "The request is invalid.";
            var request = new RestRequest(Method.GET);
            var response = client.Execute<JObject>(request);
            dynamic jsonObj = JsonConvert.DeserializeObject(response.Content);
            var error = jsonObj.Message.ToString();
            Assert.That(error.Equals(ErrorMessage));
        }

        /////////////////////////TEST-TWO//////////////////////////////////////

        [Given (@"I use API method GetTargetInfo with a PlayerID that doesn't exist")]
        public void UseGetTargetInfoBadID()
        {
            ///assuming we don't have 10000 players
            client = new RestClient("http://localhost:1842/api/Targets/GetYourTarget?id=10000");
        }
	    [Then (@"I should  recive an error")]
        public void GetErrorBadID()
        {
           
            string ErrorMessage = "An error has occurred.";
            var request = new RestRequest(Method.GET);
            var response = client.Execute<JObject>(request);
            dynamic jsonObj = JsonConvert.DeserializeObject(response.Content);
            var error = jsonObj.Message.ToString();
            Assert.That(error.Equals(ErrorMessage));

        }

        /////////////////////////TEST-THREE//////////////////////////////////////

        [Given (@"I use API method GetTargetInfo with a PlayerID that is not an int")]
        public void UseGetTargetInfoIDNotAnInt()
        {
            ///assuming we don't have 10000 players
            client = new RestClient("http://localhost:1842/api/Targets/GetYourTarget?id=A");
        }


        [Then(@"I should get an error")]
        public void GetErrorIDNotAnInt()
        {
            string ErrorMessage = "The request is invalid.";
            var request = new RestRequest(Method.GET);
            var response = client.Execute<JObject>(request);
            dynamic jsonObj = JsonConvert.DeserializeObject(response.Content);
            var error = jsonObj.Message.ToString();
            Assert.That(error.Equals(ErrorMessage));
        }


    }
}
