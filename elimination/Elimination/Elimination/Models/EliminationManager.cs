﻿using Elimination.Controllers;
using Elimination.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Elimination.Models
{
    public class EliminationManager
    {
        private readonly IRepoManager _repoManager;

        //Model specific variables
        private bool validElimination = false;
        private int targetCode = -1;
        private int playerID;
        private Player player;
        private Player eliminatedPlayer;
        private Game game;
        private string photoUrl;

        public EliminationManager(JsonElimination jsonElimination, IRepoManager repo)
        {
            this.photoUrl = jsonElimination.PhotoURL;
            this._repoManager = repo;
            this.playerID = jsonElimination.PlayerID;
            this.game = _repoManager.Games.GetGames().Where(g => g.GameID == jsonElimination.GameID).FirstOrDefault();
            this.targetCode = jsonElimination.TargetCode;
        }

        /// <summary>
        /// This method checks to see if the the targetCode provided is valid before moving on to anything else.
        /// If the targetCode is valid then it will proceed to check if the photo provided is valid as well. If the
        /// photo is valid, then the mothod will eliminate the player and return the new target. If the photo wasn't 
        /// valid, then then method will return a <seealso cref="EliminationConfirmation.Confirmation"/> with an "Error" value.
        /// </summary>
        /// <returns>A <seealso cref="EliminationConfirmation"/> with then next target if successful elimination occurs. Otherwise
        /// it will return a <seealso cref="EliminationConfirmation.Confirmation"/> with an "Error" value.
        /// </returns>
        public async Task<EliminationConfirmation> EliminatePlayer()
        {
            //Check if the targetCode is valid
            if(IsValidTargetCode())
            {
                //Convert the url provided into a byte array
                
                //Convert Base64 to byte
                byte[] img = Convert.FromBase64String(photoUrl);

                //Check to see the confidence of the photo validity
                var sf = await FacialRecognition.Compare(img, eliminatedPlayer.PhotoUrl);
                if (sf.Confidence > 0.55)
                {
                    //Photo is good to go so eliminate the player
                    this.game.EliminatePlayer(this.eliminatedPlayer.PlayerID);
                    _repoManager.Games.UpdateState(game);
                    _repoManager.SaveDb();

                    var newTarget = this.game.Targets.Where(t => t.APlayer.PlayerID == this.playerID).FirstOrDefault();
                   //var newTarget = this.game.Targets.Where(x => x.Player == this.playerID).Select(x => x.TargetPlayer).FirstOrDefault();
                    //return new target information
                    return new EliminationConfirmation
                    {
                        Confirmation = "OK",
                        NextTargetID = -1,
                        TargetName = "notrealname",
                        TargetLastName = "notrealname",
                        TargetPhoto = "notrealname",
                    };
                }
            }

            return new EliminationConfirmation
            {
                Confirmation = "Error",
                NextTargetID = -1,
                TargetName = "",
                TargetLastName = "",
                TargetPhoto = ""
            };
        }

        private bool IsValidTargetCode()
        {
            try
            {
                //Here we check to see that both players are in the game and that the targetCode is valid
                this.player = this.game.PlayersLists.Where(pl => pl.PlayerID == this.playerID).FirstOrDefault().Player;
                this.eliminatedPlayer = this.game.PlayersLists.Where(pl => pl.TargetCode == this.targetCode).FirstOrDefault().Player;

                this.validElimination = this.player != null && this.eliminatedPlayer != null;
            } 
            catch (NullReferenceException e)
            {
                return this.validElimination = false;
            }

            return this.validElimination;
        }

        private bool EliminatePlayerHelper()
        {
            if(validElimination)
            {
                //Gets the PlayerID of the player to be eliminated by going into the 
                //Players PlayersList where the GameID == this.gameID and the PlayersList targetCode == this.targetCode
                int eliminatedID = _repoManager.Players.GetAll().Where(
                    p => p.PlayersLists.All(
                        pl => pl.GameID == this.game.GameID && 
                        pl.TargetCode == this.targetCode))
                        .FirstOrDefault().PlayerID;

                this.game.EliminatePlayer(eliminatedID);
                _repoManager.Games.UpdateState(game);
                _repoManager.SaveDb();
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}