﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class RegisterUserResponse
    {
        public string Error { get; set; }
    }

    public class GeneralResponse
    {
        public string Name { get; set; }
        public string Error { get; set; }
    }
}