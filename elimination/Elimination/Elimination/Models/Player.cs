namespace Elimination.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Player : IComparable
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Player()
        {
            AttemptsEliminated = new HashSet<Attempt>();
            AttemptsEliminator = new HashSet<Attempt>();
            EliminationsEliminated = new HashSet<Elimination>();
            EliminationsEliminator = new HashSet<Elimination>();
            Games = new HashSet<Game>();
            GamesWon = new HashSet<Game>();
            PlayersLists = new HashSet<PlayersList>();
            Targets = new HashSet<Target>();
            TargetsOfPlayer = new HashSet<Target>();
            PFPlayers = new HashSet<Player>();
            PFFriends = new HashSet<Player>();
            PlayersGame = new HashSet<Game>();
            FRPlayers = new HashSet<Player>();
            FRRequestingPlayers = new HashSet<Player>();
            PlayerBadges = new HashSet<PlayerBadge>();
        }

        public int PlayerID { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(64)]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [StringLength(96)]
        public string Email { get; set; }

        [Required]
        [StringLength(20)]
        public string Phone { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; }

        public string PhotoUrl { get; set; }

        [Required]
        [StringLength(256)]
        public string Profile { get; set; }

        [Required]
        [StringLength(128)]
        public string AuthUserID { get; set; }

        /// <summary>
        /// The <seealso cref="Player"/> that escaped a tag attempt by their pursuer.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Attempt> AttemptsEliminated { get; set; }

        /// <summary>
        /// The <seealso cref="Player"/> that attempted to tag their target.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Attempt> AttemptsEliminator { get; set; }

        /// <summary>
        /// The <seealso cref="Player"/> that was eliminated by their pusuer.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Elimination> EliminationsEliminated { get; set; }

        /// <summary>
        /// The <seealso cref="Player"/> that tagged their target.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Elimination> EliminationsEliminator { get; set; }

        /// <summary>
        /// The <seealso cref="Game"/>s that this <seealso cref="Player"/> has played or is currently playing.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Game> Games { get; set; }

        /// <summary>
        /// The <seealso cref="Game"/>s that this <seealso cref="Player"/> has won.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Game> GamesWon { get; set; }

        /// <summary>
        /// A <seealso cref="ICollection{T}"/> of <seealso cref="Game"/>s and <seealso cref="Player"/>s. Each elements in the 
        /// list is a <seealso cref="Player"/> that coresponds to a <seealso cref="Game"/>.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayersList> PlayersLists { get; set; }

        /// <summary>
        /// A <seealso cref="Game"/>s Players with <seealso cref="Target"/>s.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Target> Targets { get; set; }

        /// <summary>
        /// The current <seealso cref="Player"/>s' current and past <seealso cref="Target"/>s.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Target> TargetsOfPlayer { get; set; }

        /// <summary>
        /// A <seealso cref="ICollection{T}"/> of <seealso cref="Player"/>s with friends.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Player> PFPlayers { get; set; }

        /// <summary>
        /// A <seealso cref="ICollection{T}"/> of the current <seealso cref="Player"/>s' friends. 
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Player> PFFriends { get; set; }

        /// <summary>
        /// A <seealso cref="ICollection{T}"/> of a <seealso cref="Player"/>s <seealso cref="Game"/>s.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Game> PlayersGame { get; set; }

        /// <summary>
        /// The <seealso cref="Player"/>s that this player is currently requesting to be their friend
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Player> FRPlayers { get; set; }

        /// <summary>
        /// The <seealso cref="Player"/>s requesting this <seealso cref="Player"/> to be their friend.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Player> FRRequestingPlayers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayerBadge> PlayerBadges { get; set; }

        public int CompareTo(object obj)
        {
            var player = (Player)obj;
            return String.CompareOrdinal(UserName, player.UserName);
        }
    }
}