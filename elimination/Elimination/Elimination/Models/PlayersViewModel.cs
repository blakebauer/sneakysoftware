﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class PlayersViewModel
    {
        public IEnumerable<Player> Players { get; set; }

        public Player CurrentPlayer { get; set; }
    }
}