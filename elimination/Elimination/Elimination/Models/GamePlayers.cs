﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public class GamePlayers
    {
        public Game Game { get; set; }
        public Player Host { get; set; }
        public IEnumerable<Player> RequestingPlayers { get; set; }
        public IEnumerable<Player> CurrentGamePlayers { get; set; }
        public IEnumerable<Player> AllPlayers { get; set; }
    }

    public class JsonGamePlayers
    {
        public int PlayerID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PhotoUrl { get; set; }
        public int TargetCode { get; set; }
    }
}