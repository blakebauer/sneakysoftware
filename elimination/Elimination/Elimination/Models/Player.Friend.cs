﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Elimination.Models
{
    public partial class Player
    {

        /// <summary>
        /// Adds a new <seealso cref="Player"/> friend to this player.
        /// </summary>
        /// <param name="friend">The <seealso cref="Player"/> that is to be the new friend.</param>
        /// <returns>A <seealso cref="bool"/> of true for a successful add of a friend or false 
        /// if adding a new friend wasn't successful.</returns>
        public bool AddFriend(Player friend)
        {
            /*Add the new friend to current player if the new friend doesn't already exist as a friend*/
            if (!PFFriends.Contains(friend))
            {
                PFFriends.Add(friend);

                if (!friend.PFFriends.Contains(this))
                    friend.PFFriends.Add(this);
            }
            else
                return false;

            /*Remove the friend from the FriendRequests table*/
            if (FRRequestingPlayers.Contains(friend))
                FRRequestingPlayers.Remove(friend);

            /*Remove the this player from the friends friend request list as well 
             (I don't think you need to do this step but whatever)*/
            if (friend.FRPlayers.Contains(this))
                friend.FRPlayers.Remove(this);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rejectFriend"></param>
        /// <returns></returns>
        public bool RejectFriend(Player rejectFriend)
        {
            /*Check if the rejected player is in the friend request list and remove if they are*/
            if (FRRequestingPlayers.Contains(rejectFriend))
                FRRequestingPlayers.Remove(rejectFriend);
            else
                return false;

            /*Check if the rejected players request list contains this player and remove if they are.*/
            if (rejectFriend.FRPlayers.Contains(this))
                rejectFriend.FRPlayers.Remove(this);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="possibleFriend"></param>
        /// <returns></returns>
        public bool RequestFriend(Player possibleFriend)
        {
            if (!possibleFriend.FRRequestingPlayers.Contains(this))
            {
                possibleFriend.FRRequestingPlayers.Add(this);
                FRPlayers.Add(possibleFriend);
            }
            else
            {
                return false;
            }

            return true;
        }

        public Player CancelFriendRequest(Player possibleFriend)
        {
            if (!FRRequestingPlayers.Contains(possibleFriend)) return possibleFriend;
            FRRequestingPlayers.Remove(possibleFriend);
            possibleFriend.FRPlayers.Remove(this);

            return possibleFriend;
        }
        
        public Player UnfriendPlayer(Player currentFriend)
        {
            if (!PFFriends.Contains(currentFriend)) return currentFriend;
            PFFriends.Remove(currentFriend);
            currentFriend.PFFriends.Remove(this);

            return currentFriend;
        }
    }
}