﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Elimination.Models;
using Ninject.Modules;

namespace Elimination.Repositories.Ninject
{
    public class EliminationNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IRepoManager>().To<RepoManager>().WithConstructorArgument("context",
                new EliminationDBContext());
        }
    }
}