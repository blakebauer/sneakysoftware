﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Elimination.Models;

namespace Elimination.Repositories
{
    public class PlayersListRepository : Repository<PlayersList>, IPlayersListRepository
    {
        public PlayersListRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<PlayersList> GetPlayerByID(int gID)
        {
            return EliminationDbContext.PlayersLists.Where(y => y.PlayerID.Equals(gID)).ToList();
        }

        public IEnumerable<PlayersList> GetPlayersLists()
        {
            return EliminationDbContext.PlayersLists.ToList();
        }

        public EliminationDBContext EliminationDbContext => Context as EliminationDBContext;

        internal PlayersList Get(int? id)
        {
            return EliminationDbContext.PlayersLists.Find(id);
        }

        public IEnumerable<PlayersList> GetPlayers()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<int> GetTargetCode(int g, int p)
        {
            return EliminationDbContext.PlayersLists.Where(y => y.PlayerID.Equals(p) && y.GameID.Equals(g)).Select(x=>x.TargetCode).ToList();
        }
    }
}