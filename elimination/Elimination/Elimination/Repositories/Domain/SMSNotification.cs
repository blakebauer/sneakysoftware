﻿using Elimination.Domain.Twilio;
using Elimination.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Twilio.Clients;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using WebGrease.Css.Extensions;

namespace ServerNotifications.Web.Domain
{
    public class SMSNotification
    {
        
        private readonly ITwilioRestClient restClient;
        private List<Player> players;

        public SMSNotification(List<Player> players)
        {
            this.players = players;
            restClient = new TwilioRestClient(Credentials.TwilioAccountSid, Credentials.TwilioAuthToken);
        }

        public SMSNotification(List<Player> players, ITwilioRestClient client)
        {
            this.players = players;
            restClient = client;
        }

        public async Task SendMessagesAsync(string message)
        {
            players.ForEach(async player =>
                await MessageResource.CreateAsync(
                    new PhoneNumber(player.Phone),
                    from: new PhoneNumber(Credentials.TwilioPhoneNumber),
                    body: message,
                    client: restClient));
        }
    }
}