﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using Elimination.Models;

namespace Elimination.Repositories
{
    public class PlayerBadgeRepository : Repository<PlayerBadge>, IPlayerBadgeRepository
    {
        public PlayerBadgeRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<PlayerBadge> GetPlayersBadgesByID(int p, int b)
        {
            return EliminationDbContext.PlayerBadges.Where(pb => pb.BadgeID.Equals(b) && pb.PlayerID.Equals(p)).ToList();
        }

        public IEnumerable<PlayerBadge> GetPlayerBadges()
        {
            return EliminationDbContext.PlayerBadges.Where(x => x.Badge != null).ToList();
        }


        public EliminationDBContext EliminationDbContext => Context as EliminationDBContext;



    }


}