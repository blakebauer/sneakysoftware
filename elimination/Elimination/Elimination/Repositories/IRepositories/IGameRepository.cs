﻿using Elimination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elimination.Repositories
{
    public interface IGameRepository : IRepository<Game>
    {
        string GetGameNameByID(int g);
        Game GetGameByID(int g);
        Task<Game> GetGameByIdAsync(int id);
        Task<List<Game>> GetGameListByIdsAsync(string[] ids);
        List<Game> GetGameListByIds(string[] ids);
        IEnumerable<Game> GetGamesByPlayerId(int pid);
        IEnumerable<Game> GetGames();
        IEnumerable<Game> GetAllJoinableGames();
        Game Get(int? id);
    }
}
