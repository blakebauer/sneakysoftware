﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elimination.Models;

namespace Elimination.Repositories
{
    public interface IPlayerBadgeRepository : IRepository<PlayerBadge>
    {
        IEnumerable<PlayerBadge> GetPlayersBadgesByID(int p, int b);
        IEnumerable<PlayerBadge> GetPlayerBadges();
       
    }
}
