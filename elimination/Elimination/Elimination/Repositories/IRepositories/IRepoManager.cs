﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elimination.Repositories.IRepositories;

namespace Elimination.Repositories
{
    public interface IRepoManager
    {
        string GetBadgeNameById(int id);
        IAspNetRepository Users { get; }
        IBadgeRepository Badges { get; }
        IGameRepository Games { get; }
        IPlayerRepository Players { get; }
        IPlayersListRepository PlayersLists { get; }
        IEliminationRepository Eliminations { get; }
        IPlayerBadgeRepository PlayerBadges { get; }
        ITargetRepository Targets { get; }

        DbContextTransaction BeginTransaction();
        int SaveDb();
        Task<int> SaveDbAsync();
        void Dispose();
    }
}
