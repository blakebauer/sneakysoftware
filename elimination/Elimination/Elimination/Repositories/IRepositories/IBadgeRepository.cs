﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elimination.Models;

namespace Elimination.Repositories
{
    public interface IBadgeRepository : IRepository<Badge>
    {
        IEnumerable<Badge> GetBadgesByPlayer(Player p);
        IEnumerable<Badge> GetBadgesByGame(Game g);
        bool Badge(int pID, int bID, int gID = 0);
        Badge GetBadgeByName(string badgeName);
        IEnumerable<Badge> GetBadgesById(int id);
    }
}
