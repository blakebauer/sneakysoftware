﻿using System.Web;
using System.Web.Optimization;

namespace Elimination
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery-ui-1.12.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            //bundles.Add(new ScriptBundle("~/bundles/dragdrop").Include(
            //            "~/Scripts/drag-drop.js"));

            bundles.Add(new ScriptBundle("~/bundles/elimination").Include(
                        "~/Scripts/elimination.js"));

            bundles.Add(new ScriptBundle("~/bundles/accordion").Include(
                        "~/Scripts/accordion.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/bs-select").Include(
                      "~/Scripts/bootstrap-select.js"));

            bundles.Add(new ScriptBundle("~/bundles/playerEdit").Include(
                "~/Scripts/playerEdit.js"));

            bundles.Add(new ScriptBundle("~/bundles/invitePlayers").Include(
                      "~/Scripts/invitePlayers.js"));

            bundles.Add(new ScriptBundle("~/bundles/ms-select").Include(
                      "~/Scripts/msdropdown/jquery.dd.js"));

            bundles.Add(new ScriptBundle("~/bundles/friendRequests").Include(
                      "~/Scripts/friendRequests.js"));

            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
                        "~/Scripts/bootstrap-notify.min.js",
                        "~/Scripts/dashboard.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-select.min.css",
                      "~/Content/msdropdown/*.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/animate").Include(
                      "~/Content/animate.css"));

            bundles.Add(new StyleBundle("~/Content/datepicker").Include(
                      "~/Content/themes/base/datepicker.css"));

            bundles.Add(new StyleBundle("~/Content/jqueryui").Include(
                      "~/Content/themes/base/jquery-ui.min.css"));
        }
    }
}
