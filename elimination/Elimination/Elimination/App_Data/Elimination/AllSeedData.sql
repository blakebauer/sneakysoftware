﻿USE [Elimination]

:r "./Identity/dbo.AspNetUsers.data.sql"
GO

:r "./Elimination/dbo.Players.data.sql"
GO

:r "./Elimination/dbo.Games.data.sql"
GO

:r "./Elimination/dbo.PlayersLists.data.sql"
GO

:r "./Elimination/dbo.Badges.data.sql"
GO

:r "./Elimination/dbo.PlayerBadges.data.sql"
GO

:r "./Elimination/dbo.Targets.data.sql"
GO

:r "./Elimination/dbo.Eliminations.data.sql"
GO

:r "./Elimination/dbo.PlayersFriends.data.sql"
GO

:r "./Elimination/dbo.RequestingPlayers.data.sql"
GO

:r "./Elimination/dbo.FriendRequests.data.sql"
GO