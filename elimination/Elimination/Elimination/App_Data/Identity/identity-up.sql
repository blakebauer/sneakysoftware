﻿USE [Elimination]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AspNetRoles](
	[Id]		[NVARCHAR](128)	NOT NULL
	,[Name]		[NVARCHAR](256) NOT NULL
	,CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
	( [Id] ASC ) WITH ( PAD_INDEX = OFF
	,STATISTICS_NORECOMPUTE = OFF
	,IGNORE_DUP_KEY = OFF
	,ALLOW_ROW_LOCKS = ON
	,ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int]		IDENTITY(1,1)			NOT NULL
	,[UserId]		[NVARCHAR](128)			NOT NULL
	,[ClaimType]	[NVARCHAR](max)			NULL
	,[ClaimValue]	[NVARCHAR](max)			NULL
	,CONSTRAINT		[PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
	( [Id] ASC ) WITH ( PAD_INDEX = OFF
	,STATISTICS_NORECOMPUTE = OFF
	,IGNORE_DUP_KEY = OFF
	,ALLOW_ROW_LOCKS = ON
	,ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider]		[NVARCHAR](128) NOT NULL
	,[ProviderKey]		[NVARCHAR](128) NOT NULL
	,[UserId]			[NVARCHAR](128) NOT NULL
	,CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
	([LoginProvider]	ASC
	,[ProviderKey]		ASC
	,[UserId]			ASC )WITH (PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId]	[NVARCHAR](128) NOT NULL,
	[RoleId]	[NVARCHAR](128) NOT NULL,
	CONSTRAINT	[PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
	( [UserId] ASC, [RoleId] ASC )WITH (PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AspNetUsers](
	[Id]					[NVARCHAR](128) NOT NULL
	,[Email]				[NVARCHAR](256) NULL
	,[EmailConfirmed]		[BIT]			NOT NULL
	,[PasswordHash]			[NVARCHAR](max) NULL
	,[SecurityStamp]		[NVARCHAR](max) NULL
	,[PhoneNumber]			[NVARCHAR](max) NULL
	,[PhoneNumberConfirmed]	[BIT]			NOT NULL
	,[TwoFactorEnabled]		[BIT]			NOT NULL
	,[LockoutEndDateUtc]	[DATETIME]		NULL
	,[LockoutEnabled]		[BIT]			NOT NULL
	,[AccessFailedCount]	[INT]			NOT NULL
	,[UserName]				[NVARCHAR](256) NOT NULL
	,CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
	( [Id] ASC )WITH (PAD_INDEX = OFF
		,STATISTICS_NORECOMPUTE = OFF
		,IGNORE_DUP_KEY = OFF
		,ALLOW_ROW_LOCKS = ON
		,ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO

ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO

ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO