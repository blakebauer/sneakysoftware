﻿/**
 * Deals with friend request stuff
 **/


/*Calls a method in the DashboardController to add a new friend to the current player*/
function acceptFriendRequest(playerId, playerFriendId) {
    console.log("Accepting player as friend with playerID of: " + playerFriendId);

    var urlAddress = "Players/AcceptFriendRequest";

    /*Make ajax call to Dashboard controller to accept player as friend*/
    $.ajax({
        type: "POST"
        , datatype: "JSON"
        , url: urlAddress
        , data: { playerId: playerId, friendRequesterId: playerFriendId }
        , success: function (response) {
            if (response[0] === "OK") {
                var data = [playerFriendId, response[1]];
                removeFriendRequestItemFromList(data);
            }
            else if (response[0] === "error") {
                console.log("An error occured when trying to add the player with PlayerID of " + playerFriendId + " as a new friend");
            }
            else if (response[0] === "nullid") {
                console.log("A null id was passed. playerID is: " + playerId + " and playerFriendID is: " + playerFriendId);
            }
        }
        , error: function (response) {
            console.log("In error function with response of: " + response[0]);
        }
    });
}


/**
 * 
 * @param {any} playerId
 * @param {any} toBeFriend
 */
function rejectFriendRequest(playerId, toBeFriend) {
    console.log("Rejecting player with ID of: " + toBeFriend);

    var urlAddress = "Players/RejectFriendRequest";

    /*Make ajax call to reject player*/
    $.ajax({
        type: "POST"
        , datatype: "JSON"
        , url: urlAddress
        , data: { playerId: playerId, toBeFriend: toBeFriend }
        , success: function (response) {
            console.log("rejected player");
            if (response[0] === "OK") {
                var data = [toBeFriend, response[1]];
                removeFriendRequestItemFromList(data);
            }
            else if (response[0] === "error") {
                console.log("An error occured when trying to reject the player with PlayerID of " + toBeFriend + " as a new friend");
            }
            else if (response[0] === "nullid") {
                console.log("A null id was passed to reject player function. playerID is: " + playerId + " and playerFriendID is: " + toBeFriend);
            }
        }
        , error: function (response) {
            console.log("In error function of reject player with response of: " + response[0]);
        }
    });
}


/**
 * 
 * @param {any} playerId
 * @param {any} toBeFriend
 */
function sendFriendRequest(playerId, toBeFriend) {
    console.log("Sending friend request to player with ID of: " + toBeFriend);

    var urlAddress = "Players/RequestAFriend";

    /*Make ajax call to reject player*/
    $.ajax({
        type: "POST"
        , datatype: "JSON"
        , url: urlAddress
        , data: { playerId: playerId, toBeFriend: toBeFriend }
        , success: function (response) {
            if (response[0] === "OK") {
                console.log("Disabling button with id of " + response[1]);
                $("#request-friend-button-" + response[1]).attr("disabled", "disabled");
                $("#retract-request-button-" + response[1]).removeAttr("disabled");
            }
            else if (response[0] === "error") {
                console.log("An error occured when trying to request a friend with PlayerID of " + toBeFriend);
            }
            else if (response[0] === "nullid") {
                console.log("A null id was passed to request a friend function. playerID is: " + playerId + " and playerFriendID is: " + toBeFriend);
            }
        }
        , error: function (response) {
            console.log("In error function of request friend with response of: " + response[0]);
        }
    });
}


/**
 * 
 * @param {any} playerId
 * @param {any} toBeFriendRetract
 */
function cancelFriendRequest(playerId, toBeFriendRetract) {
    console.log("Rejecting player with ID of: " + toBeFriendRetract);

    var urlAddress = "Players/CancelAFriendRequest";

    /*Make ajax call to reject player*/
    $.ajax({
        type: "POST"
        , datatype: "JSON"
        , url: urlAddress
        , data: { playerId: playerId, toBeFriendRetract: toBeFriendRetract }
        , success: function (response) {
            if (response[0] === "OK") {

                $("#retract-request-button-" + response[1]).attr("disabled", "disabled");
                $("#request-friend-button-" + response[1]).removeAttr("disabled");
            }
            else if (response[0] === "error") {
                console.log("An error occured when trying to reject the player with PlayerID of " + toBeFriendRetract + " as a new friend");
            }
            else if (response[0] === "nullid") {
                console.log("A null id was passed to reject player function. playerID is: " + playerId + " and playerFriendID is: " + toBeFriendRetract);
            }
        }
        , error: function (response) {
            console.log("In error function of reject player with response of: " + response[0]);
        }
    });
}

/**
 * 
 * @param {any} playerId
 * @param {any} toBeUnfriended
 */
function unfriendPlayer(playerId, toBeUnfriended) {
    console.log("Rejecting player with ID of: " + toBeUnfriended);

    $("#dialog").dialog({
        buttons: {
            "Confirm": function () {
                callUnfriendPlayerMethod(playerId, toBeUnfriended);
                $(this).dialog("close");
            },
            "Cancel": function () {
                $(this).dialog("close");
            }
        }
    });

    $("#dialog").dialog("open");
}

function callUnfriendPlayerMethod(playerId, toBeUnfriended) {
    var urlAddress = "Players/UnfriendPlayer";

    /*Make ajax call to reject player*/
    $.ajax({
        type: "POST",
        datatype: "JSON",
        url: urlAddress,
        data: { playerId: playerId, toBeUnfriended: toBeUnfriended },
        success: function (response) {
            if (response[0] === "OK") {
                var data = [toBeUnfriended, response[1]];
                removeFriendFromFriendsList(data);
            } else if (response[0] === "error") {
                console.log("An error occured when trying to reject the player with PlayerID of " +
                    toBeUnfriended +
                    " as a new friend");
            } else if (response[0] === "nullid") {
                console.log("A null id was passed to reject player function. playerID is: " +
                    playerId +
                    " and playerFriendID is: " +
                    toBeUnfriended);
            }
        },
        error: function (response) {
            console.log("In error function of reject player with response of: " + response[0]);
        }
    });
}

/**
 * 
 * @param {any} data
 */
function removeFriendFromFriendsList(data) {
    console.log("Going to remove player with id of " + data[0]);
    $("#friends_div_" + data[0]).remove();
}


/**
 * removes an item from the friend request menu by the players ID.
 * @param {any} id The players ID
 */
function removeFriendRequestItemFromList(data) {

    console.log("Going to remove player with id of " + data[0]);
    $("#div_" + data[0]).remove();
    $("span#friendRequestBadge.badge").text(data[1]);
}

/**
 * Confirmation dialog
 */
$(document).ready(function () {
    $("#dialog").dialog({
        autoOpen: false,
        modal: true
    });
});
