For people who want to manage a schedule for their phone calls, the TaskMe app allows 
users to set up tasks that will manage phone calls such as denying incoming calls at 
set times, certain people calling, certain events happening, or other conditions such 
as the state of the phone. Unlike current apps that manage tasks such as Automate and Tasker, 
they are overly complex for the general user. The way that these apps work is by setting 
up tasks based on programming like logical conditions that are complicated to understand 
for the general user. TaskMe would simplify these tasks by setting up the most common things 
for the user by default based on what type of task it is. The TaskMe app will create task 
and store them in a database locally, the user will have the option to backup these task 
in the cloud in case they lose their device or accidentally uninstall the app. 