﻿-- Exported from QuickDBD: https://www.quickdatatabasediagrams.com/
-- Link to schema: https://app.quickdatabasediagrams.com/#/schema/GP4uKzQva0GgrxkrgcNNtg
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.


CREATE TABLE "Tasks" (
    "TaskID" INT GENERATED ALWAYS AS IDENTITY NOT NULL ,
    "TaskName" VARCHAR(40)  NOT NULL ,
    CONSTRAINT "pk_Tasks" PRIMARY KEY (
        "TaskID"
    )
)

GO

CREATE TABLE "Dates" (
    "DateID" INT GENERATED ALWAYS AS IDENTITY NOT NULL ,
    "StartDate" DateTime  NOT NULL ,
    "EndDate" DateTime  NOT NULL ,
    "TaskID" INT  NOT NULL ,
    CONSTRAINT "pk_Dates" PRIMARY KEY (
        "DateID"
    )
)

GO

CREATE TABLE "TaskActions" (
    "TActionID" INT GENERATED ALWAYS AS IDENTITY NOT NULL ,
    "TActionName" VARCHAR(50)  NOT NULL ,
    "TaskID" INT  NOT NULL ,
    CONSTRAINT "pk_TaskActions" PRIMARY KEY (
        "TActionID"
    )
)

GO

CREATE TABLE "TaskParameters" (
    "TActionID" INT  NOT NULL ,
    "ActionID" INT  NOT NULL ,
    "TParameters" VARCHAR(MAX)  NOT NULL ,
    CONSTRAINT "pk_TaskParameters" PRIMARY KEY (
        "TActionID","ActionID"
    )
)

GO

CREATE TABLE "TaskUsers" (
    "ContactID" INT  NOT NULL ,
    "TActionID" INT  NOT NULL ,
    CONSTRAINT "pk_TaskUsers" PRIMARY KEY (
        "ContactID","TActionID"
    )
)

GO

CREATE TABLE "Actions" (
    "ActionID" INT GENERATED ALWAYS AS IDENTITY NOT NULL ,
    "ActionName" VARCHAR(50)  NOT NULL ,
    "ActionDesc" VARCAHR(400)  NOT NULL ,
    "ActionMethod" VARCHAR(50)  NOT NULL ,
    CONSTRAINT "pk_Actions" PRIMARY KEY (
        "ActionID"
    )
)

GO

CREATE TABLE "Contacts" (
    "ContactID" INT GENERATED ALWAYS AS IDENTITY NOT NULL ,
    "FirstName" VARCHAR(50)  NOT NULL ,
    "LastName" VARCHAR(50)  NOT NULL ,
    "Email" VARCHAR(150)  NULL ,
    "Phone" VARCHAR(30)  NOT NULL ,
    "Photo" IMAGE  NULL ,
    CONSTRAINT "pk_Contacts" PRIMARY KEY (
        "ContactID"
    )
)

GO

ALTER TABLE "Tasks" ADD CONSTRAINT "fk_Tasks_TaskID" FOREIGN KEY("TaskID")
REFERENCES "TaskActions" ("TaskID")
GO

ALTER TABLE "Dates" ADD CONSTRAINT "fk_Dates_TaskID" FOREIGN KEY("TaskID")
REFERENCES "Tasks" ("TaskID")
GO

ALTER TABLE "TaskParameters" ADD CONSTRAINT "fk_TaskParameters_TActionID" FOREIGN KEY("TActionID")
REFERENCES "TaskActions" ("TActionID")
GO

ALTER TABLE "TaskParameters" ADD CONSTRAINT "fk_TaskParameters_ActionID" FOREIGN KEY("ActionID")
REFERENCES "Actions" ("ActionID")
GO

ALTER TABLE "TaskUsers" ADD CONSTRAINT "fk_TaskUsers_ContactID" FOREIGN KEY("ContactID")
REFERENCES "Contacts" ("ContactID")
GO

ALTER TABLE "TaskUsers" ADD CONSTRAINT "fk_TaskUsers_TActionID" FOREIGN KEY("TActionID")
REFERENCES "TaskActions" ("TActionID")
GO

